from flask import Flask, render_template, jsonify, request
import json


app = Flask(__name__)

db = []

with open('data.json') as file:
    snacks = json.load(file)
    db = snacks


@app.get('/')
def home_page():
    my_data = {'title': 'Snack Service'}
    # Don't forget to add a 'templates' folder! Flask uses it by default for templates.
    return render_template('index.html', page=my_data)


@app.get('/api/snacks/')
def all_snacks():
    return jsonify(db)


@app.get('/api/snacks/<int:snack_id>')
def get_snack(snack_id):
    for snack in db:
        if snack['id'] == snack_id:
            return jsonify(snack)
    return jsonify({})


@app.post('/api/snacks/')
def create_snack():
    last_snack_id = db[-1].get('id')
    
    new_snack = request.data
    new_snack['id'] = last_snack_id+1
    db.append(new_snack)
    return jsonify(new_snack)

